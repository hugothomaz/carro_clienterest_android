package hugothomaz.com.carrorest.presenter;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import java.io.ByteArrayOutputStream;
import hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp.IModelCarSaveRest;
import hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp.IPresenterCarSave;
import hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp.IViewCarSave;
import hugothomaz.com.carrorest.model.ModelCarSaveImpl;
import hugothomaz.com.carrorest.model.bus.MessageBus;
import hugothomaz.com.carrorest.model.pojo.Car;

/**
 * Created by Hugo on 27/03/2017.
 */

public class PresenterCarSaveImpl implements IPresenterCarSave {

    private IModelCarSaveRest modelCarSave;
    private IViewCarSave view;

    private Context context;

    private Car car;
    private Bitmap bitmap = null;
    private byte[] arrayImageCar = null;

    boolean permissionCamera = false;




    public PresenterCarSaveImpl(){
        modelCarSave = new ModelCarSaveImpl(this);
    }


    @Override
    public void setView(IViewCarSave view) {
        this.view = view;
    }

    //-----------------------Talking Model--------------------------------------------
    //Envia carro para o servidor
    @Override
    public void onSendCar(Car car) {
        car.setPhoto(arrayImageCar);

        modelCarSave.onSendCar(car);

        Log.i("database_tag", "saveCar AddCarActivity - id: ");

        //Temos que usar o EventBus para enviar informação do id salvo de volta
        //temos também de arrumar uma forma do jax-rs retornar o id para nos aqui no cliente
        MessageBus msg = new MessageBus(null, MessageBus.FLAG_ADD_CAR, car);
        eventOfSendingMessageOfBusForVeiculosModeloSelected(msg);
        view.finish();
    }

    @Override
    public Context setContext(Context context) {
        return this.context;
    }


    //---------------------------Talking View-------------------------------------------
    //Eventos de tela

    @Override
    public boolean verificarCampos(TextInputEditText edModel, TextInputLayout tilModel, TextInputEditText edMarca, TextInputLayout tilMarca, TextInputEditText edDescricao, TextInputLayout tilDescricao) {

        int cont = 3;

        if(edModel.getText().toString().equals("")){
            tilModel.setError("Informe o Model");
            cont--;
        }
        if(edMarca.getText().toString().equals("")){
            tilMarca.setError("Informe a Marca");
            cont--;
        }
        if(edDescricao.getText().toString().equals("")){
            tilDescricao.setError("Informe uma Descrição");
            cont--;
        }


        if (cont == 3){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void clear() {
        car = null;
        bitmap = null;
        arrayImageCar = null;
    }


    @Override
    public Bitmap onResultBitmatByCamera(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            Bundle bundle = data.getExtras();
            Log.i("app", "data não esta vazio");
            if (bundle != null) {
                Log.i("app", "Bundle não esta vazio");
                this.bitmap = (Bitmap) bundle.get("data");

                if (bitmap != null) {
                    ByteArrayOutputStream rotuloSaida = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, rotuloSaida);
                    arrayImageCar = rotuloSaida.toByteArray();
                    return bitmap;

                }

            }
        }
        return null;
    }

    //Eventos da camera
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.i("camera", "onRequestPermissionsResult");
        boolean permissao = false;
        switch (requestCode) {
            case 0: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    view.openCloseContainerImage(null);
                    permissionCamera = true;
                    Log.i("camera", "onRequestPermissionsResult - Foi consedida Permissão, permissionCamera = " + permissionCamera);


                } else {
                    permissionCamera = false;
                    Log.i("camera", "onRequestPermissionsResult - Não foi consedida Permissão, permissionCamera = " + permissionCamera);


                }
                break;
            }
        }

    }

    @Override
    public boolean permissionOk() {
        return permissionCamera;
    }

    @Override
    public boolean hasCamera() {
        PackageManager pm = context.getPackageManager();
        if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else{
            return false;
        }
    }


    @Override
    public boolean isViewAttached() {
        return view != null;
    }


    @Subscribe
    public void handleReceivetOfBusByCarDetailFragment(MessageBus mBus){

    }


    public void eventOfSendingMessageOfBusForVeiculosModeloSelected(MessageBus mBus){
        EventBus.getDefault().post(mBus);
    }

}
