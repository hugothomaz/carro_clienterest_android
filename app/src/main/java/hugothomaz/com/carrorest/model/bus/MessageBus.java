package hugothomaz.com.carrorest.model.bus;


import hugothomaz.com.carrorest.model.pojo.Car;

/**
 * Created by Hugo on 17/03/2017.
 */

public class MessageBus {

    public static final int FLAG_FAG_FAV = 55484;
    public static final int FLAG_ADD_CAR = 58584;
    public static final int FLAG_DET_CAR = 95254;

    private Car car;

    //Flag para dizer oque desejamos
    private int flag = -1;
    private Class classSolicitante;

/*

PresenterCarSaveImpl: O carro que queremos passar para outro Fragment
Flag: Texto para localizar a intenção do Bus, para aonde deve ir
 */


    public MessageBus(Class classSolicitante, int flag, Car car) {
        this.classSolicitante = classSolicitante;
        this.flag = flag;
        this.car = car;
    }


    public MessageBus(Class classSolicitante, int flag) {
        this.classSolicitante = classSolicitante;
        this.flag = flag;
    }



    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }


    public Class getaClass() {
        return classSolicitante;
    }

    public void setaClass(Class classSolicitante) {
        this.classSolicitante = classSolicitante;
    }
}
