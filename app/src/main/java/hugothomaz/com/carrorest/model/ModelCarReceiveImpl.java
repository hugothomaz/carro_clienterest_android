package hugothomaz.com.carrorest.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp.IModelCarReceiveRest;
import hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp.IModelCarSaveRest;
import hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp.IPresenterCarSave;
import hugothomaz.com.carrorest.interfaces.connectInterfaces.apiDataConsumer.CarAPI;
import hugothomaz.com.carrorest.model.pojo.Car;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hugo on 27/03/2017.
 */

public class ModelCarReceiveImpl implements IModelCarReceiveRest {

    private List<Car> resultCarros;

    private IPresenterCarSave presenterCar;


    public ModelCarReceiveImpl(IPresenterCarSave presenterCar) {
        this.presenterCar = presenterCar;
    }

    @Override
    public List<Car> onReceiveAllCar() {

        Log.i("CarAllFragment", "onConnectWebServiceByRetrofity");
        CarAPI carAPI = CarAPI.retrofit.create(CarAPI.class);

        Call<List<Car>> callCars;
        callCars = carAPI.getCarAll();

        callCars.enqueue(new Callback<List<Car>>() {
            @Override
            public void onResponse(Call<List<Car>> call, Response<List<Car>> response) {
                List<Car> carros = new ArrayList<Car>();

                if(response.body() != null){
                    carros.addAll(response.body());

                    if(carros != null){
                        resultCarros = carros;
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Car>> call, Throwable t) {
                Log.i("CarAllFragment", "Falha ao baixar dados do servidor. Message: " + t.getMessage());
            }
        });

        if(resultCarros!=null){
            return resultCarros;
        }else{
            return null;
        }
    }


    //-----------------------------------------------------------------------------------------------
    @Override
    public Car onReceiveCar() {
        return null;
    }


}
