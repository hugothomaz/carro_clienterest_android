package hugothomaz.com.carrorest.model.deserializer;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import hugothomaz.com.carrorest.model.pojo.Car;

/**
 * Created by Hugo on 18/02/2017.
 */

public class CarDeserializer implements JsonDeserializer<Object>{


    @Override
    public Object deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonElement car = json.getAsJsonObject();

        if(json.getAsJsonObject().get("car") != null){
            car = json.getAsJsonObject().get("car");
        }

        return (new Gson().fromJson(car, Car.class));
    }
}
