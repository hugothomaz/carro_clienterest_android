package hugothomaz.com.carrorest.model.pojo;

import java.io.Serializable;



public class Car implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String model;
	private String brand;
	private String description;
	private Boolean favorite;
	private byte[] photo;


    public Car() {
    }

    public Car(String model, String brand, String description, Boolean favorite, byte[] photo) {
		this.model = model;
		this.brand = brand;
		this.description = description;
		this.favorite = favorite;
		this.photo = photo;
	}


	public Car(Integer id, String model, String brand, String description, Boolean favorite, byte[] photo) {
		this.id = id;
		this.model = model;
		this.brand = brand;
		this.description = description;
		this.favorite = favorite;
		this.photo = photo;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getFavorite() {
		return favorite;
	}

	public void setFavorite(Boolean favorite) {
		this.favorite = favorite;
	}


	@Override
	public String toString() {
		return "PresenterCarSaveImpl{" +
				"id=" + id +
				", model='" + model + '\'' +
				", brand='" + brand + '\'' +
				", description='" + description + '\'' +
				", favorite=" + favorite +
				", photo=" + photo +
				'}';
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


}
