package hugothomaz.com.carrorest.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp.IModelCarAlterRest;
import hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp.IModelCarSaveRest;
import hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp.IPresenterCarSave;
import hugothomaz.com.carrorest.interfaces.connectInterfaces.apiDataConsumer.CarAPI;
import hugothomaz.com.carrorest.model.pojo.Car;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hugo on 27/03/2017.
 */

public class ModelCarAlterImpl implements IModelCarAlterRest {


    @Override
    public void onUpdateFavoriteCar() {

    }

    @Override
    public void onUpdateCar() {

    }
}
