package hugothomaz.com.carrorest.model.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import hugothomaz.com.carrorest.view.fragment.CarAllFragment;
import hugothomaz.com.carrorest.view.fragment.CarFavoriteFragment;

/**
 * Created by Hugo on 21/02/2017.
 */

public class TabCarAdapter extends FragmentPagerAdapter {

    private Context context;
    private String[] titles = {"TODOS", "FAVORITOS"};

    public TabCarAdapter(FragmentManager fm, Context c) {
        super(fm);

        context = c;
    }




    @Override
    public Fragment getItem(int position) {
        Fragment frag = null;
        if(position == 0){
            Log.i("TabAdapter", "new CarAllFragment");
            frag = new CarAllFragment();
        }
        else if (position == 1){
            Log.i("TabAdapter", "new CarFavoriteFragment");
            frag = new CarFavoriteFragment();
        }


        return frag;
    }

    @Override
    public int getCount() {

        return titles.length;
    }



    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }








}








