package hugothomaz.com.carrorest.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp.IModelCarReceiveRest;
import hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp.IModelCarSaveRest;
import hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp.IPresenterCarSave;
import hugothomaz.com.carrorest.interfaces.connectInterfaces.apiDataConsumer.CarAPI;
import hugothomaz.com.carrorest.model.pojo.Car;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hugo on 27/03/2017.
 */

public class ModelCarSaveImpl implements IModelCarSaveRest {

    IPresenterCarSave presenterCarSave;


    public ModelCarSaveImpl(IPresenterCarSave presenterCarSave){
        this.presenterCarSave = presenterCarSave;
    }

    @Override
    public void onSendCar(Car car) {
        CarAPI carAPI = CarAPI.retrofit.create(CarAPI.class);

        Call<ResponseBody> sendCar;
        sendCar = carAPI.sendNewCar(car);

        sendCar.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i("onConnectWebServiceByRetrofity", "carro salvo ?");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i("onConnectWebServiceByRetrofity", "falha ao tentar salvar");
            }
        });
    }
}
