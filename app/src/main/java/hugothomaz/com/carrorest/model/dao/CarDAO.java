package hugothomaz.com.carrorest.model.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import hugothomaz.com.carrorest.model.pojo.Car;

/**
 * Created by Hugo on 07/03/2017.
 */

public class CarDAO extends DAO {


    private static final String APP_TAG_DATABASE = "database_tag";

    public CarDAO(Context context) {
        super(context);
    }


    // ------------- Cria um Item vindo de um cursor que foi obtido de uma Query ------------------
    //------------------- Usado normalmente em Métodos de Consulta -------------------------
    private Car criarCar(Cursor cursor){


        Car car = new Car(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.ID)),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.MODEL)),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.BRAND)),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.DESCRIPTION)),
                cursor.getInt(cursor.getColumnIndex(DataBaseHelper.FAVORITE))==0?false:true,
                cursor.getBlob(cursor.getColumnIndex(DataBaseHelper.PHOTO))
                );
        return car;
    }

    // ---------------- Cria um contentVelue apartir de um Item--------------------------
    //-------------------Usado em métodos de insert, update para mostrar o item que deve ser manipulado----
    private ContentValues contentValuesCar(Car car){
        ContentValues ctv = new ContentValues();
        ctv.put(DataBaseHelper.ID, car.getId());
        ctv.put(DataBaseHelper.MODEL, car.getModel());
        ctv.put(DataBaseHelper.BRAND, car.getBrand());
        ctv.put(DataBaseHelper.DESCRIPTION, car.getDescription());
        ctv.put(DataBaseHelper.FAVORITE, car.getFavorite()==false?0:1);
        ctv.put(DataBaseHelper.PHOTO, car.getPhoto());

        return ctv;
    }



    public int insert(Car car) throws SQLException{
        int result;
        try {
            result = (int) getDatabase().insert(DataBaseHelper.TABLE, null, contentValuesCar(car));
            Log.i(APP_TAG_DATABASE, "Carro inserido com sucesso");
            return result;
        }catch (SQLException e){
            Log.e(APP_TAG_DATABASE, "Falha ao inserir Carro " + e.getMessage());
            throw new SQLException("Falha ao inserir Carro " + e.getMessage());
        }

    }


    public int update(Car car){
        int result;
        try{
            result = getDatabase().update(DataBaseHelper.TABLE, contentValuesCar(car), DataBaseHelper.ID + "=?", new String[]{car.getId().toString()});
            Log.i(APP_TAG_DATABASE, "Carro atualizado com sucesso");
            return result;
        }catch (SQLException e){
            Log.e(APP_TAG_DATABASE, "Falha ao atualizar Carro " + e.getMessage());
            throw new SQLException("Falha ao atualizar Carro " + e.getMessage());
        }
    }

    public int favOnFavOff(Integer id){
        int result;
        try{
            Car car = new Car();
            car = getCar(id);
            boolean fav = car.getFavorite();
            if (car.getFavorite()==true){
                car.setFavorite(false);
            }else{
                car.setFavorite(true);
            }

            result = getDatabase().update(DataBaseHelper.TABLE, contentValuesCar(car), DataBaseHelper.ID + "=?", new String[]{id.toString()});
            Log.i(APP_TAG_DATABASE, "Carro ID: " + car.getId() + ", Modelo: " + car.getModel() + " era " + fav + " passou para " + car.getFavorite());
            return result;
        }catch (SQLException e){
            Log.e(APP_TAG_DATABASE, "Falha ao alterar FavCar " + e.getMessage());
            throw new SQLException("Falha ao alterar FavCar " + e.getMessage());
        }
    }

    public int delete(Car car) throws SQLException{
        int result;
        try{
            result = getDatabase().delete(DataBaseHelper.TABLE, DataBaseHelper.ID + "=?", new String[]{car.getId().toString()});
            Log.i(APP_TAG_DATABASE, "Carro deletado com sucesso");
            return result;
        }catch (SQLException e){
            Log.e(APP_TAG_DATABASE, "Falha ao deletar carro " + e.getMessage());
            throw new SQLException("Falha ao deletar carro: " + e.getMessage());
        }
    }


    public Car getCar(Integer id){
        Cursor cursor = getDatabase().query(DataBaseHelper.TABLE, DataBaseHelper.COLUNAS_CAR,
                DataBaseHelper.ID + "=?", new String[]{id.toString()}, null, null, null);

        if(cursor.moveToFirst()){
            Car car = criarCar(cursor);
            cursor.close();
            return car;
        }
        return null;
    }


    public List<Car> getAllCar(){
        Cursor cursor = getDatabase().query(DataBaseHelper.TABLE, DataBaseHelper.COLUNAS_CAR,
                null, null, null, null, null);

        //getDatabase().close();

        ArrayList<Car> listCarros = new ArrayList<Car>();
        while (cursor.moveToNext()){
            Car item = criarCar(cursor);
            listCarros.add(item);
        }
        cursor.close();

        return listCarros;
    }


    public List<Car> getAllCarFav(){
        Cursor cursor = getDatabase().query(DataBaseHelper.TABLE, DataBaseHelper.COLUNAS_CAR,
                DataBaseHelper.FAVORITE + "=?", new String[]{"1"}, null, null, null);

        //getDatabase().close();

        ArrayList<Car> listCarros = new ArrayList<Car>();
        while (cursor.moveToNext()){
            Car item = criarCar(cursor);
            listCarros.add(item);
        }
        cursor.close();
        Log.i("app", "Lista " + listCarros.size());
        return listCarros;
    }


}
