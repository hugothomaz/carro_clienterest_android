package hugothomaz.com.carrorest.model.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import hugothomaz.com.carrorest.R;
import hugothomaz.com.carrorest.model.pojo.Car;

/**
 * Created by Hugo on 21/02/2017.
 */

public class RecyclerCarAdapter extends RecyclerView.Adapter<RecyclerCarAdapter.ViewHolderCar>{

    private List<Car> mList;
    private LayoutInflater mLayoutInflater;

    private byte[] arrayImageCar;

    public RecyclerCarAdapter(Context c, List<Car> mList) {
        Log.i("RecyclerAdapter", "RecyclerCarAdapter");
        this.mList = mList;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolderCar onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.mold_card_car, parent, false);
        ViewHolderCar viewHolder = new ViewHolderCar(view);
        Log.i("RecyclerAdapter", "onCreateViewHolder");

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolderCar holder, int position) {
        int imgFavIcon;
        Log.i("RecyclerAdapter", "onBindViewHolder");

        arrayImageCar = mList.get(position).getPhoto();
        if(arrayImageCar!=null){
            Bitmap image = BitmapFactory.decodeByteArray(arrayImageCar, 0, arrayImageCar.length);
            holder.imageViewCar.setImageBitmap( image );
        }


        if (mList.get(position).getFavorite()==true){
            imgFavIcon = R.mipmap.favorite_selected;
        }else{
            imgFavIcon = R.mipmap.favorite_noselected;
        }

        holder.buttonFavCar.setImageResource(imgFavIcon);

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void addListItem(Car c, int position){
        Log.i("RecyclerAdapter", "addListItem");
        mList.add(c);
        notifyItemInserted(position);
    }

    public void removeListItem(int position){
        mList.remove(position);
        notifyItemRemoved(position);
    }

    public class ViewHolderCar extends RecyclerView.ViewHolder {

        public ImageView imageViewCar;
        public ImageView buttonFavCar;

        public ViewHolderCar(View itemView) {
            super(itemView);
            Log.i("RecyclerAdapter - ViewHolderCar", "ViewHolderCar");
            imageViewCar = (ImageView) itemView.findViewById(R.id.imageViewRecyclerCar);
            buttonFavCar = (ImageView) itemView.findViewById(R.id.imageViewRecyclerButtonFavCar);

        }



    }
}
