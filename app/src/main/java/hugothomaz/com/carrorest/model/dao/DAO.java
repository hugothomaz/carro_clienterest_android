package hugothomaz.com.carrorest.model.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Hugo on 18/02/2017.
 */

public abstract class DAO {

    private SQLiteOpenHelper mOpenHelper;
    private SQLiteDatabase mDataBase;


    public DAO(Context context){
        mOpenHelper = new DataBaseHelper(context);
    }


    protected SQLiteDatabase getDatabase(){
        if (mDataBase == null){
            mDataBase = mOpenHelper.getWritableDatabase();
        }
        return mDataBase;
    }

    public void close(){
        mDataBase.close();
        mOpenHelper.close();
    }

}
