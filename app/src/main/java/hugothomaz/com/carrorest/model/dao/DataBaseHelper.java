package hugothomaz.com.carrorest.model.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Hugo on 07/03/2017.
 */

public class DataBaseHelper extends SQLiteOpenHelper {

    private Context mContext;
    private static final String BANCO_DADOS = "carros_sqlite";
    public static final String TABLE = "carro";
    private static final int VERSAO_DB_ATUAL = 100;

    public static final String ID = "id";
    public static final String MODEL = "modelo";
    public static final String BRAND = "brand";
    public static final String DESCRIPTION = "description";
    public static final String FAVORITE = "favorite";
    public static final String PHOTO = "photo";

    public static final String[] COLUNAS_CAR = new String[]{ID, MODEL,
            BRAND, DESCRIPTION, FAVORITE, PHOTO };


    public DataBaseHelper(Context context){
        super(context, BANCO_DADOS, null, VERSAO_DB_ATUAL);
        this.mContext = context;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL("CREATE TABLE ["+TABLE+"](\n" +
                "    ["+ ID +"] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, \n" +
                "    ["+ MODEL +"] VARCHAR(120) NOT NULL, \n" +
                "    ["+ BRAND +"] VARCHAR(120) NOT NULL, \n" +
                "    ["+ DESCRIPTION +"] VARCHAR(520), \n" +
                "    ["+ FAVORITE +"] BOOLEAN NOT NULL DEFAULT 0, \n" +
                "    ["+ PHOTO +"] BLOB);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
