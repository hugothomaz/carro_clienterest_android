package hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;



import java.util.List;

import hugothomaz.com.carrorest.model.bus.MessageBus;
import hugothomaz.com.carrorest.model.pojo.Car;

/**
 * Created by Hugo on 18/02/2017.
 */

public interface IPresenterCarSave {



        //Receber a View
        //Método somenta do Presenter
        public void setView(IViewCarSave view);


        //--------------------------------------------------------------------
        //Talking View - Model
        //Envia carro para o servidor
        public void onSendCar(Car car);
        public Context setContext(Context context);


        //--------------------------------------------------------------------
        //Talking View
        //Eventos de tela
        public void clear();


        //--------------------------------------------------------------------
        //Eventos da camera
        public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults);
        public boolean permissionOk();
        public boolean hasCamera();
        public boolean isViewAttached();
        public boolean verificarCampos(TextInputEditText edModel, TextInputLayout tilModel, TextInputEditText edMarca, TextInputLayout tilMarca, TextInputEditText edDescricao, TextInputLayout tilDescricao);
        public Bitmap onResultBitmatByCamera(int requestCode, int resultCode, Intent data);



        //--------------------------------------------------------------------
        //Eventos de click, camera e tela
        //public void openCamera(); //Abre a camera
        //public void openCloseContainerImage(); //Abre o container que tera a imagem capturada

}
