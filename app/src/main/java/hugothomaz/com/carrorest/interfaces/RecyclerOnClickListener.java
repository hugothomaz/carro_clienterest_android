package hugothomaz.com.carrorest.interfaces;

import android.view.View;

/**
 * Created by Hugo on 22/02/2017.
 */

public interface RecyclerOnClickListener {

    public void onClickListener(View v, int position);
    public void onClickLongListener(View v, int position);


}
