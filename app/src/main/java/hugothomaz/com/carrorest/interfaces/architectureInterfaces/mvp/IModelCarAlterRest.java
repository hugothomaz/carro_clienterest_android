package hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp;

/**
 * Created by Hugo on 31/03/2017.
 */

public interface IModelCarAlterRest {
    public void onUpdateFavoriteCar();
    public void onUpdateCar();
}
