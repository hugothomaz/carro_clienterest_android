package hugothomaz.com.carrorest.interfaces.connectInterfaces.apiDataConsumer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import hugothomaz.com.carrorest.model.deserializer.CarDeserializer;
import hugothomaz.com.carrorest.model.pojo.Car;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by Hugo on 18/02/2017.
 */

public interface CarAPI {


    //Recupera todos os Carros
    @GET("car")
    Call<List<Car>> getCarAll();

    //Recupera todos os carros favoritos
    @GET("car/fav")
    Call<List<Car>> getCarFavorite();

    //Atualiza o estados de um determinado veiculo, muda true false do favorito
    @PUT("car/fav/{id}")
    void setFavCarTrueOrFalse(@Path("id") int id);

    //Envia PresenterCarSaveImpl para o WebService
    @POST("car")
    Call<ResponseBody> sendNewCar(@Body Car car);


    Gson gson = new GsonBuilder().registerTypeAdapter(Car.class, new CarDeserializer()).create();


    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://192.168.25.2:8080/CarFavoriteWS-0.0.1-SNAPSHOT/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

}
