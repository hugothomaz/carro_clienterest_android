package hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp;

import hugothomaz.com.carrorest.model.pojo.Car;

/**
 * Created by Hugo on 31/03/2017.
 */

public interface IModelCarSaveRest {

        //Envia carro para o servidor
        public void onSendCar(Car car);
}
