package hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp;

import android.view.View;

/**
 * Created by Hugo on 18/02/2017.
 */

public interface IViewCarAll {

    int REQUEST_NEW_CAR = 584;

    //Talking Presenter
    //Envia carro para o servidor
    public void saveCar(View veiw);

    //Eventos de tela
    // public void preencherCampos(Car car);
    public boolean verificarCampos();

    //public void clear();

    //Eventos da camera
    void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults);
    //public void verificaPermissaoCamera();
    // Não precisa verificar na view se tem permissão pois vamos verificar isso somenta no presenter e
    //caso nao tenha vamos emitir uma mensagem


    // public void hasCamera();
    //nao precisa do hasCamera no View pois vai ser chamado diretamente pelo opencamera no presenter

    //Eventos de click, camera e tela
    public void openCamera(View veiw); //Abre a camera
    public void openCloseContainerImage(View veiw); //Abre o container que tera a imagem capturada
    public void finish();
    public void showToast(String msg, int toastDuration);











}
