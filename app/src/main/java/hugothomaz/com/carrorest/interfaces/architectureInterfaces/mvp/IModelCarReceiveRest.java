package hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp;

import java.util.List;
import java.util.concurrent.ExecutionException;

import hugothomaz.com.carrorest.interfaces.connectInterfaces.apiDataConsumer.CarAPI;
import hugothomaz.com.carrorest.model.pojo.Car;

/**
 * Created by Hugo on 18/02/2017.
 */

public interface IModelCarReceiveRest {
    public List<Car> onReceiveAllCar();
    public Car onReceiveCar();
}
