package hugothomaz.com.carrorest.view.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


import hugothomaz.com.carrorest.R;
import hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp.IPresenterCarSave;
import hugothomaz.com.carrorest.interfaces.architectureInterfaces.mvp.IViewCarSave;
import hugothomaz.com.carrorest.model.bus.MessageBus;
import hugothomaz.com.carrorest.model.pojo.Car;
import hugothomaz.com.carrorest.presenter.PresenterCarSaveImpl;

public class AddCarActivity extends AppCompatActivity  implements IViewCarSave{

    private static final int REQUEST_NEW_CAR = 584;

    private IPresenterCarSave presenterCarSave;


    private ViewGroup mlayout_add_item;
    private Toolbar toolbar;

    boolean btCameraVisible = false;

    private TextView btnOpenCloseContainerImage;
    private CardView containerImageCar;
    private ImageView btnAbrirCamera;

    private TextInputLayout tilModel;
    private TextInputLayout tilMarca;
    private TextInputLayout tilDescricao;

    private TextInputEditText edModelo;
    private TextInputEditText edMarca;
    private TextInputEditText edDescricao;
    private SwitchCompat swFavorito;



    private ImageButton btSaveCar;
/*
    private Car car;
    private Bitmap bitmap = null;
    private byte[] arrayImageCar = null;

    //private CarDAO carDAO;

    boolean permissionCamera = false;
    boolean btCameraVisible = false;
*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_car);

        presenterCarSave = new PresenterCarSaveImpl();
        presenterCarSave.setView(this);
        presenterCarSave.setContext(getBaseContext());



        mlayout_add_item = (ViewGroup) findViewById(R.id.mlayout_add_car);

        toolbar = (Toolbar) findViewById(R.id.toolbar_add_car);
        toolbar.setTitle("Novo Carro");

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnOpenCloseContainerImage = (TextView) findViewById(R.id.tv_add_detalhes_rotulo);
        containerImageCar = (CardView) findViewById(R.id.container_add_image_car);
        btnAbrirCamera = (ImageView) findViewById(R.id.iv_add_rotulo_item);

        edModelo = (TextInputEditText) findViewById(R.id.edt_detail_add_modelo);
        edMarca = (TextInputEditText) findViewById(R.id.edt_detail_add_marca);
        edDescricao = (TextInputEditText) findViewById(R.id.edt_detail_add_descricao);
        swFavorito = (SwitchCompat) findViewById(R.id.switch_detail_add_fav);

        tilModel = (TextInputLayout) findViewById(R.id.textInputLayout);
        tilMarca = (TextInputLayout) findViewById(R.id.textInputLayout2);
        tilDescricao = (TextInputLayout) findViewById(R.id.textInputLayout3);

        btSaveCar = (ImageButton) findViewById(R.id.btnSalvar);

        swFavorito.setTextOn("Sim");
        swFavorito.setTextOff("Não");

    }




    //------------------------------- Menus -------------------------------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflaterMenu = getMenuInflater();
        inflaterMenu.inflate(R.menu.menu_add_car, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.action_add_car_limpar :
                clear();
                return true;

            case android.R.id.home :
                finish();
                return true;

            default: break;
        }

        return super.onOptionsItemSelected(item);
    }


    //------------------------------- Salvando Carro -------------------------------------
    @Override
    public void saveCar(View veiw){
        Car car = new Car();
        car.setModel(edModelo.getText().toString());
        car.setBrand(edMarca.getText().toString());
        car.setDescription(edDescricao.getText().toString());
        car.setFavorite(swFavorito.isChecked());
        //car.setPhoto(arrayImageCar);

        if(verificarCampos()){
            presenterCarSave.onSendCar(car);
        }
    }


    //------------------------------- Verifica se os campos não estão vazios -------------------------------------
    @Override
    public boolean verificarCampos(){
        return presenterCarSave.verificarCampos(edModelo, tilModel, edMarca, tilMarca, edDescricao, tilDescricao);
    }


    //------------------------------- Limpa os campos, variaveis e objetos -------------------------------------
    public void clear(){
        edModelo.setText("");
        edModelo.requestFocus();
        edMarca.setText("");
        edDescricao.setText("");
        presenterCarSave.clear();
    }



    //------------------------------- Pede permissão para usar a camera -------------------------------------
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[],
                                           int[] grantResults) {

        presenterCarSave.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }
    //------------------------------- Abre a camera -------------------------------------

    @Override
    public void openCamera(View veiw) {
        if (presenterCarSave.hasCamera() && presenterCarSave.permissionOk()) {
            Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(i, 0);
        }else{
            showToast("Não possuí câmera", Toast.LENGTH_LONG);
        }
    }




    //------------------------------- Abre o Container do botão para abrir a
    @Override
    public void openCloseContainerImage(View veiw){
        if (btCameraVisible == false && presenterCarSave.permissionOk()) {
            containerImageCar.setVisibility(View.VISIBLE);
            btCameraVisible = true;
        } else if (btCameraVisible == true) {
            containerImageCar.setVisibility(View.GONE);
            btCameraVisible = false;
        } else if(!presenterCarSave.permissionOk()){
            showToast("Você não tem permissão", Toast.LENGTH_LONG);
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        final Bitmap bitmap = presenterCarSave.onResultBitmatByCamera(requestCode, resultCode, data);

        Log.i("app", "onActivityResult");

        if (bitmap != null) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //ivBtCameraAdd.setImageResource();
                    btnAbrirCamera.setImageBitmap(bitmap);
                }
            });
        }
    }


    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void showToast(String msg, int toastDuration) {
       Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

    }



    @Subscribe
    public void handleReceivetOfBusByCarDetailFragment(MessageBus mBus){
        Log.i("CarDetailActivity handleReceivetOfBusByCarDetailFragment", mBus.getCar().getModel().toString());
    }


    public void eventOfSendingMessageOfBusForVeiculosModeloSelected(MessageBus mBus){
        EventBus.getDefault().post(mBus);
    }

}
