package hugothomaz.com.carrorest.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import hugothomaz.com.carrorest.R;
import hugothomaz.com.carrorest.interfaces.RecyclerOnClickListener;
import hugothomaz.com.carrorest.interfaces.connectInterfaces.apiDataConsumer.CarAPI;
import hugothomaz.com.carrorest.model.adapter.RecyclerCarAdapter;
import hugothomaz.com.carrorest.model.bus.MessageBus;
import hugothomaz.com.carrorest.model.pojo.Car;
import hugothomaz.com.carrorest.view.activity.AddCarActivity;
import hugothomaz.com.carrorest.view.activity.CarDetailActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hugo on 17/02/2017.
 */

public class CarFavoriteFragment extends Fragment implements RecyclerOnClickListener {

    private static final int REQUEST_NEW_CAR = 584;

    private RecyclerView mRecyclerView;
    private RecyclerCarAdapter mAdapter;
    private int sizeList;
    private List<Car> mListCars;
    private FloatingActionButton fab;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //super.onCreateView(inflater, container, savedInstanceState);

        EventBus.getDefault().register(this);

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.getSupportActionBar().setTitle("Carros Fav");

        View view = inflater.inflate(R.layout.fragment_car_all, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_car_all);
        mRecyclerView.setHasFixedSize(true);

        fab = (FloatingActionButton) getActivity().findViewById(R.id.fab_add_car);


        // Linerar Padrão
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(llm);

        onConnectWebServiceByRetrofity();

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(dy > 0){
                    fab.hide();
                }else{
                    fab.show();
                }
            }
        });


        mRecyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getActivity(), mRecyclerView, this));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getActivity(), AddCarActivity.class), REQUEST_NEW_CAR);
            }
        });
        return view;
    }





    private void onConnectWebServiceByRetrofity() {
        CarAPI carAPI = CarAPI.retrofit.create(CarAPI.class);

        Call<List<Car>> callCars;
        callCars = carAPI.getCarFavorite();

        callCars.enqueue(new Callback<List<Car>>() {
            @Override
            public void onResponse(Call<List<Car>> call, Response<List<Car>> response) {
                List<Car> carrosFav = new ArrayList<Car>();

                if(response.body()!=null){
                    carrosFav.addAll(response.body());
                    if (carrosFav!=null){
                        sizeList = carrosFav.size();
                        mListCars = carrosFav;
                        onResumeRecyclerView(carrosFav);
                    }
                }

            }

            @Override
            public void onFailure(Call<List<Car>> call, Throwable t) {

            }
        });
    }




    private void onResumeRecyclerView(List<Car> list){
        mAdapter = new RecyclerCarAdapter(getActivity(), list);
        mRecyclerView.setAdapter(mAdapter);
    }




    @Override
    public void onClickListener(View v, int position) {
        MessageBus mBus = new MessageBus(this.getClass(), MessageBus.FLAG_DET_CAR, mListCars.get(position));
        eventOfSendingMessageOfBusForVeiculosModeloSelected(mBus);
        startActivity(new Intent(getActivity(), CarDetailActivity.class));
    }

    @Override
    public void onClickLongListener(View v, int position) {

    }


    @Subscribe
    public void handleReceivetOfBusByCarDetailFragment(MessageBus mBus){

    }




    public void eventOfSendingMessageOfBusForVeiculosModeloSelected(MessageBus mBus){
        EventBus.getDefault().postSticky(mBus);
    }




    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }




    // ainda sera reaporveitado com o eventbus
    //no momento nao esta sendo utilizado
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("CarAllFragment", "onActivityResult");

        if(requestCode == REQUEST_NEW_CAR && resultCode == Activity.RESULT_OK){
            Log.i("CarAllFragment", "onActivityResult request e result code batem");
            int id = -1;
            id = data.getIntExtra("NEW_CAR_OK", -2);

            if (id!=-1 && id!=-2){
                Log.i("CarAllFragment", "onActivityResult - PresenterCarSaveImpl recebeu o car salvo no banco com o id recebido e adapter add novo item na lista");
                /*
                Aqui vamos receber o id do carro que acabou de ser salvo na Activity AddCarActivity
                e vamos buscar no método que vamos criar para buscar no REST e vamos adicionar na lista do
                RecyclerView como ja estavamos fazendo quando usavamos o DB.

                PresenterCarSaveImpl car = carDAO.getCar(id);
                Log.i("CarAllFragment", "onActivityResult - addListItem");
                */
                mAdapter.addListItem(null, sizeList);
            }else if(id==-2){
                Log.i("CarAllFragment", "onActivityResult - ID recebido " + id );
            }



        }
    }





    //-----------------------------------------------------------------------------------------------------
    //---------------------------------RecyclerViewTouchListener-------------------------------------------
    //-----------------------------------------------------------------------------------------------------
    public static class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener{

        private GestureDetector mGestureDetector;

        public RecyclerViewTouchListener(Context c, final RecyclerView r, final RecyclerOnClickListener click) {

            mGestureDetector = new GestureDetector(c, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    View v = r.findChildViewUnder(e.getX(), e.getY());
                    if(v!=null && click != null){
                        click.onClickListener(v, r.getChildAdapterPosition(v));
                    }

                    return true;
                }
            }


            );
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            mGestureDetector.onTouchEvent(e);
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {


        }
    }








}
