package hugothomaz.com.carrorest.view.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import hugothomaz.com.carrorest.R;
import hugothomaz.com.carrorest.model.bus.MessageBus;
import hugothomaz.com.carrorest.model.dao.CarDAO;
import hugothomaz.com.carrorest.model.pojo.Car;
import hugothomaz.com.carrorest.view.fragment.CarAllFragment;
import hugothomaz.com.carrorest.view.fragment.CarFavoriteFragment;

public class CarDetailActivity extends AppCompatActivity {

    Toolbar mToolBar;
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    AppBarLayout appbar;

    private ImageView iv_carro;
    private ImageView iv_fav;
   // private TextView tv_modelo;
    private TextView tv_marca;
    private TextView tv_descricao;

    private CarDAO carDao;

    private List<Car> mList = new ArrayList<>();
    private Car mCar;

    private ImageView mIV_Fav;

    private byte[] arrayImageCar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_detail);



        mIV_Fav = (ImageView) findViewById(R.id.iv_detalhe_fav);
        mToolBar = (Toolbar) findViewById(R.id.toolbar_detalhe_carro);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_car_detail);
        appbar = (AppBarLayout) findViewById(R.id.appbar);



        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        iv_carro = (ImageView) findViewById(R.id.iv_detalhe_carro);
        iv_fav = (ImageView) findViewById(R.id.iv_detalhe_fav);
        //tv_modelo = (TextView) findViewById(R.id.tv_detalhe_modelo);
        tv_marca = (TextView) findViewById(R.id.tv_detalhe_marca);
        tv_descricao = (TextView) findViewById(R.id.tv_detalhe_descricao_carro);

        carDao = new CarDAO(getBaseContext());
        mList = carDao.getAllCar();






        appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if(verticalOffset!=0){
                    Log.i("app", "AppBar maior que 0");
                    iv_fav.setVisibility(View.INVISIBLE);
                }

                if(verticalOffset==0){
                    Log.i("app", "AppBar igual 0");
                    iv_fav.setVisibility(View.VISIBLE);
                }


            }
        });






    }

    @Override
    protected void onResume() {
        super.onResume();

        EventBus.getDefault().register(CarDetailActivity.this);



    }


    //TODO: Depois passar o preenchimento desses campos para a método original preencherCampos
    @Subscribe(threadMode = ThreadMode.POSTING, sticky = true)
    public void handleReceivetOfBusByCarDetailFragmentEvent(MessageBus mBus){
        Log.i("CarDetailActivity handleReceivetOfBusByCarDetailFragment", mBus.getCar().getModel().toString());
        if(mBus!=null && mBus.getFlag()!=-1){
            if(mBus.getFlag()==MessageBus.FLAG_DET_CAR && mBus.getaClass()==CarAllFragment.class
                    || mBus.getaClass()== CarFavoriteFragment.class){
                Car car = mBus.getCar();
                if(car!=null){
                    mCar = car;
                    if(car!=null){
                        if(car.getPhoto()!=null){
                            arrayImageCar = car.getPhoto();
                            Bitmap image = BitmapFactory.decodeByteArray(arrayImageCar, 0, arrayImageCar.length);
                            iv_carro.setImageBitmap(image);
                        }

                        mCollapsingToolbarLayout.setTitle(car.getModel().toString());
                        iv_fav.setImageResource(car.getFavorite()?R.mipmap.favorite_selected:R.mipmap.favorite_noselected);
                        //  tv_modelo.setText(mCar.getModel());
                        tv_marca.setText(car.getBrand());
                        tv_descricao.setText(car.getDescription());
                    }
                }
            }


        }
    }


    public void eventOfSendingMessageOfBusForVeiculosModeloSelected(MessageBus mBus){
        EventBus.getDefault().post(mBus);
    }


private void preencherCampos(Car car){

}


    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(CarDetailActivity.this);
    }
}
