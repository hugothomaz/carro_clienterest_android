package hugothomaz.com.carrorest.view.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import hugothomaz.com.carrorest.R;
import hugothomaz.com.carrorest.model.adapter.TabCarAdapter;
import hugothomaz.com.carrorest.model.tab.SlidingTabLayout;

/**
 * Created by Hugo on 17/02/2017.
 */

public class DashboardActivity extends AppCompatActivity {

   // private FragmentManager mFragmentManager;
  //  private FragmentTransaction mFragmentTransaction;
//   private CarAllFragment carAllFragment;

    private Toolbar mToolBar;

    private Drawer mNavigationDrawer;
    private AccountHeader mAccountHeader;

    private SlidingTabLayout mSlidingTabLayoutCars;
    private ViewPager mViewPagerCars;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        mToolBar = (Toolbar) findViewById(R.id.toolbar_dashboard);
        mToolBar.setTitle("App Carros");
        setSupportActionBar(mToolBar);


        // Fragment
       // mFragmentManager = getSupportFragmentManager();
      //  if(savedInstanceState == null){
      //      mFragmentManager = getSupportFragmentManager();
          //  mFragmentTransaction = mFragmentManager.beginTransaction();

         //   carAllFragment = new CarAllFragment();

        //    mFragmentTransaction.add(R.id.dashboard_frag_container, carAllFragment, "car_all");
        //    mFragmentTransaction.commit();
      //  }



        // TABS
    //    mViewPagerCars = (ViewPager) findViewById(R.id.view_pager_tabs_cars);
     //   mSlidingTabLayoutCars = (SlidingTabLayout) findViewById(R.id.sliding_tabs_cars);


        // Tabs
        mViewPagerCars = (ViewPager) findViewById(R.id.view_pager_tab_cars);
        mViewPagerCars.setAdapter(new TabCarAdapter(getSupportFragmentManager(), this));

        mSlidingTabLayoutCars = (SlidingTabLayout) findViewById(R.id.stl_tab_cars);
        mSlidingTabLayoutCars.setDistributeEvenly(true);
        mSlidingTabLayoutCars.setHorizontalScrollBarEnabled(true);
        mSlidingTabLayoutCars.setHorizontalFadingEdgeEnabled(true);
        mSlidingTabLayoutCars.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mSlidingTabLayoutCars.setSelectedIndicatorColors(getResources().getColor(R.color.accent));
        mSlidingTabLayoutCars.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                int i = position;
                mNavigationDrawer.setSelection(i+2);
                if(position==0){
                    animateTitleChange("Carros");
                }
                else if(position == 1){
                    animateTitleChange("Carros Fav");
                }
                else if(position == 2){
                    animateTitleChange("Carros Notícias");
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mSlidingTabLayoutCars.setViewPager(mViewPagerCars);




        // Navigation
        mAccountHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withCompactStyle(false)
                .withSavedInstance(savedInstanceState)
                .withThreeSmallProfileImages(false)


                .addProfiles(new ProfileDrawerItem()
                        .withName("PresenterCarSaveImpl Trilha Tiengo")
                        .withIcon(R.drawable.logo_nav_drawer)
                )
                .build();


        mNavigationDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(mToolBar)
                .withDisplayBelowStatusBar(true)
                .withActionBarDrawerToggleAnimated(true)
                .withDrawerGravity(Gravity.LEFT)
                .withSavedInstance(savedInstanceState)
                .withSelectedItem(2)
                .withDelayOnDrawerClose(290)
                .withDisplayBelowStatusBar(true)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if(position>1 && position<5){
                            mViewPagerCars.setCurrentItem(position-2);
                        }

                        return false;
                    }
                })
                .withOnDrawerItemLongClickListener(new Drawer.OnDrawerItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(View view, int position, IDrawerItem drawerItem) {
                        Toast.makeText(DashboardActivity.this, "OnItemLongClick Navigation Drawer", Toast.LENGTH_LONG).show();
                        return false;
                    }
                })
                .withAccountHeader(mAccountHeader)
                .build();



            mNavigationDrawer.addItem(new PrimaryDrawerItem()
                                        .withName("Principal")
                                        .withIconColorRes(R.color.material_drawer_dark_selected));
            mNavigationDrawer.addItem(new PrimaryDrawerItem()
                                        .withName("Todos os Carros")
                                        .withIcon(R.mipmap.all_cars));

            mNavigationDrawer.addItem(new PrimaryDrawerItem()
                                        .withName("Todos Favoritos")
                                        .withIcon(R.mipmap.favorite_noselected)
                                        .withSelectedIcon(R.mipmap.favorite_selected));

            mNavigationDrawer.addItem(new PrimaryDrawerItem()
                                        .withName("Noticias")
                                        .withIcon(R.mipmap.favorite_noselected)
                                        .withSelectedIcon(R.mipmap.favorite_selected));

            mNavigationDrawer.addItem(new DividerDrawerItem());

            mNavigationDrawer.addItem(new PrimaryDrawerItem()
                                        .withName("Sobre")
                                        .withIconColorRes(R.color.material_drawer_dark_selected));







    }


/*
    public List<PresenterCarSaveImpl> getSetCarList(int qtd){
        String[] models = new String[] {"Gallardo", "Pagani Zonda", "Cruze", "Golf", "Impala", "BMW 720i", "Mustang", "Camaro", "Focus", "Peugeot 308", "DB77"};
        String[] brands = new String[] {"Lamborghini", "Pagani", "Chevrolet", "VW", "Chevrolet", "BMW", "Ford", "Chevrolet", "Ford", "Peugeot", "Aston Martin"};
        int[] photos = new int[]{R.drawable.lamborghini, R.drawable.pagani, R.drawable.cruze, R.drawable.golf, R.drawable.impala, R.drawable.bmw750, R.drawable.mustang, R.drawable.camaro, R.drawable.focus, R.drawable.paugeot308, R.drawable.db77};
        Boolean[] favorite = new Boolean[]{false, false, true, false, true, true, true, false, true, true, false};

        String[] description =
                new String[]{"Automobili Lamborghini S.p.A é uma fabricante italiana de automóveis desportivos de luxo e de alto desempenho criada originalmente para competir com a Ferrari com sede no município Modena de Sant'Agata Bolognese.",
                             "O Zonda é um carro esportivo construído pela fabricante italiana Pagani. Ele estreou em 1999, e a produção terminou em 2011, com três carros de edições especiais, o Zonda 760RS, Zonda 760LH e o Zonda 764 Passione, que foram produzidos em 2012. Até junho de 2009, 135 Zondas tinha sido construídos, incluindo mulas de desenvolvimento.",
                             "O Chevrolet Cruze é um sedã projetado pela General Motors(GM) em conjunto com a Coreana Daewoo. Há também no Japão um crossover compacto com esse nome. Chegou ao Brasil em setembro de 2011 e substituiu o Chevrolet Vectra a outras caras novas, como Citroën C4 Lounge (CAR Awards 2014[1]), Peugeot 408, Renault Fluence, Honda Civic, Hyundai Elantra, Toyota Corolla, Volkswagen Jetta."};

        List<PresenterCarSaveImpl> listAux = new ArrayList<>();

        for(int i = 0; i < qtd; i++){
            PresenterCarSaveImpl c = new PresenterCarSaveImpl(models[i % models.length], brands[i % brands.length], description[i % description.length], favorite[i % favorite.length], photos[i % photos.length]);
            listAux.add(c);
        }
        return listAux;
    }
*/




    private View getToolbarTitle() {
        int childCount = mToolBar.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = mToolBar.getChildAt(i);
            if (child instanceof TextView) {
                return child;
            }
        }

        return new View(this);
    }



    private void animateTitleChange(final String newTitle) {
        final View view = getToolbarTitle();



        if (view instanceof TextView) {
            AlphaAnimation fadeOut = new AlphaAnimation(1f, 0f);
            fadeOut.setDuration(250);
            fadeOut.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mToolBar.setTitle(newTitle);

                    AlphaAnimation fadeIn = new AlphaAnimation(0f, 1f);
                    fadeIn.setDuration(100);
                    view.startAnimation(fadeIn);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            view.startAnimation(fadeOut);
        }
    }

}
