package hugothomaz.com.carrorest.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import hugothomaz.com.carrorest.R;
import hugothomaz.com.carrorest.interfaces.RecyclerOnClickListener;
import hugothomaz.com.carrorest.interfaces.connectInterfaces.apiDataConsumer.CarAPI;
import hugothomaz.com.carrorest.model.adapter.RecyclerCarAdapter;
import hugothomaz.com.carrorest.model.bus.MessageBus;
import hugothomaz.com.carrorest.model.pojo.Car;
import hugothomaz.com.carrorest.view.activity.AddCarActivity;
import hugothomaz.com.carrorest.view.activity.CarDetailActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hugo on 17/02/2017.
 */

public class CarAllFragment extends Fragment implements RecyclerOnClickListener{

    private static final int REQUEST_NEW_CAR = 584;

    private RecyclerView mRecyclerCarAll;
    private int sizeList;
    private List<Car> mListCars;
    private RecyclerCarAdapter adapter;
    private FloatingActionButton fab;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_car_all, container, false);
        Log.i("CarAllFragment", "onCreateView");



        mRecyclerCarAll = (RecyclerView) view.findViewById(R.id.recycler_car_all);
        mRecyclerCarAll.setHasFixedSize(true);

        fab = (FloatingActionButton) getActivity().findViewById(R.id.fab_add_car);


/**
        // Linerar Padrão
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerCarAll.setLayoutManager(llm);
**/

/*
        // Linear Reverso
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        llm.setReverseLayout(true);
        mRecyclerCarAll.setLayoutManager(llm);
*/

       /*
        // GridLayout
        GridLayoutManager glm = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
        mRecyclerCarAll.setLayoutManager(glm);
*/

        StaggeredGridLayoutManager sglm = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        sglm.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
        mRecyclerCarAll.setLayoutManager(sglm);


        Log.i("CarAllFragment", "Chamando o onConnectWebServiceByRetrofity");
        onConnectWebServiceByRetrofity();


        mRecyclerCarAll.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(dy > 0){
                    fab.hide();
                }else{
                    fab.show();
                }


                /*
                LinearLayoutManager glm = (LinearLayoutManager) mRecyclerCarAll.getLayoutManager();
                RecyclerCarAdapter adapter = (RecyclerCarAdapter) mRecyclerCarAll.getAdapter();

                if(mList.size() == glm.findLastCompletelyVisibleItemPosition() + 1){
                    List<PresenterCarSaveImpl> listAux = ((DashboardActivity) getActivity()).getSetCarList(10);
                    for (int i = 0; i < listAux.size(); i++){
                        adapter.addListItem(listAux.get(i), mList.size());
                    }
                }
                */

                /*
                GridLayoutManager glm = (GridLayoutManager) mRecyclerCarAll.getLayoutManager();
                RecyclerCarAdapter adapter = (RecyclerCarAdapter) mRecyclerCarAll.getAdapter();

                if(mList.size() == glm.findLastCompletelyVisibleItemPosition() + 1){
                    List<PresenterCarSaveImpl> listAux = ((DashboardActivity) getActivity()).getSetCarList(10);
                    for (int i = 0; i < listAux.size(); i++){
                        adapter.addListItem(listAux.get(i), mList.size());
                    }
                }
                */

                /*
                StaggeredGridLayoutManager sglm = (StaggeredGridLayoutManager) mRecyclerCarAll.getLayoutManager();
                int[] aux = sglm.findFirstCompletelyVisibleItemPositions(null);
                int max = -1;
                for(int i = 0; i < aux.length; i++){
                    max = aux[i] > max ? aux[i] : max;
                }

                if(mList.size() == max + 1){
                    List<PresenterCarSaveImpl> listAux = carDAO.getAllCar();
                    for (int i = 0; i < listAux.size(); i++){
                        adapter.addListItem(listAux.get(i), mList.size());
                    }
                }

*/
            }
        });


        mRecyclerCarAll.addOnItemTouchListener(new RecyclerViewTouchListener(getActivity(), mRecyclerCarAll, this));



        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  MessageBus mBus = new MessageBus(this.getClass(), MessageBus.FLAG_ADD_CAR);
               // handleReceivetOfBusByCarDetailFragment(mBus);

                //Não precisa de um Bus para enviar para AddCar porque se ela responder com algo tanto para Fav quando para All
                // teremos que atualizar a lista de qualquer forma
                startActivity(new Intent(getActivity(), AddCarActivity.class));
            }
        });

       /** fab.attachToRecyclerView(mRecyclerCarAll, new ScrollDirectionListener() {
            @Override
            public void onScrollDown() {

            }

            @Override
            public void onScrollUp() {

            }
        } );

        fab.setOnClickListener(this);
        **/



        return view;
    }


    private void onConnectWebServiceByRetrofity() {



        Log.i("CarAllFragment", "onConnectWebServiceByRetrofity");
        CarAPI carAPI = CarAPI.retrofit.create(CarAPI.class);

        Call<List<Car>> callCars;
        callCars = carAPI.getCarAll();

        callCars.enqueue(new Callback<List<Car>>() {
            @Override
            public void onResponse(Call<List<Car>> call, Response<List<Car>> response) {
                List<Car> carros = new ArrayList<Car>();

                if(response.body() != null){
                    carros.addAll(response.body());

                    if(carros != null){
                        for(Car car  : carros){
                            Log.i("app", car.getModel().toString());
                        }
                        sizeList = carros.size();
                        mListCars = carros;
                        onResumeRecyclerView(carros);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Car>> call, Throwable t) {
                Log.i("CarAllFragment", "Falha ao baixar dados do servidor");
            }
        });

    }


    // Chama a CarDetailActivity com o carro que foi clicado
    @Override
    public void onClickListener(View v, int position) {
        Log.i("CarAll onClickListener -> CarDetailActivity", mListCars.get(position).getModel().toString());
        MessageBus mBus = new MessageBus(this.getClass(), MessageBus.FLAG_DET_CAR, mListCars.get(position));
        eventOfSendingMessageOfBusForVeiculosModeloSelected(mBus);
        startActivity(new Intent(getActivity(), CarDetailActivity.class));
    }


    @Override
    public void onClickLongListener(View v, int position) {
        Toast.makeText(getActivity(), "Click Longo", Toast.LENGTH_LONG).show();
    }

    private void onResumeRecyclerView(List<Car> listCars){
        Log.i("CarAllFragment", "onResumeRecyclerView");
        adapter = new RecyclerCarAdapter(getActivity(), listCars);
        mRecyclerCarAll.setAdapter(adapter);
    }


    /**
     @Override
     public void onResume() {
     super.onResume();
     Log.i("CarAllFragment", "onResume");
     }

     @Override
     public void onAttach(Context context) {
     super.onAttach(context);
     Log.i("CarAllFragment", "onAttach");
     }

     @Override
     public void onDestroy() {
     super.onDestroy();
     Log.i("CarAllFragment", "onDestroy");
     }

     @Override
     public void onDestroyView() {
     super.onDestroyView();
     Log.i("CarAllFragment", "onDestroyView");
     }

     @Override
     public void onCreate(@Nullable Bundle savedInstanceState) {
     super.onCreate(savedInstanceState);
     Log.i("CarAllFragment", "onCreate");
     }


     @Override
     public void onActivityCreated(@Nullable Bundle savedInstanceState) {
     super.onActivityCreated(savedInstanceState);
     Log.i("CarAllFragment", "onActivityCreated");
     }

     @Override
     public void onStart() {
     super.onStart();
     Log.i("CarAllFragment", "onStart");
     }

     @Override
     public void onPause() {
     super.onPause();
     Log.i("CarAllFragment", "onPause");
     }

     @Override
     public void onStop() {
     super.onStop();
     Log.i("CarAllFragment", "onStop");
     }




     @Override
     public void onDetach() {
     super.onDetach();
     Log.i("CarAllFragment", "onDetach");
     }

     **/




    @Subscribe(threadMode = ThreadMode.POSTING)
    public void handleReceivetOfBusByCarDetailFragment(MessageBus mBus){
        if(mBus!=null && mBus.getFlag()!=-1){
            Car car = mBus.getCar();
        }
    }


    public void eventOfSendingMessageOfBusForVeiculosModeloSelected(MessageBus mBus){
        EventBus.getDefault().postSticky(mBus);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(CarAllFragment.this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(CarAllFragment.this);
    }

    // ainda sera reaporveitado com o eventbus
    //no momento nao esta sendo utilizado
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("CarAllFragment", "onActivityResult");

        if(requestCode == REQUEST_NEW_CAR && resultCode == Activity.RESULT_OK){
            Log.i("CarAllFragment", "onActivityResult request e result code batem");
            int id = -1;
            id = data.getIntExtra("NEW_CAR_OK", -2);

            if (id!=-1 && id!=-2){
                Log.i("CarAllFragment", "onActivityResult - PresenterCarSaveImpl recebeu o car salvo no banco com o id recebido e adapter add novo item na lista");
                /*
                Aqui vamos receber o id do carro que acabou de ser salvo na Activity AddCarActivity
                e vamos buscar no método que vamos criar para buscar no REST e vamos adicionar na lista do
                RecyclerView como ja estavamos fazendo quando usavamos o DB.

                PresenterCarSaveImpl car = carDAO.getCar(id);
                Log.i("CarAllFragment", "onActivityResult - addListItem");
                */
                adapter.addListItem(null, sizeList);
            }else if(id==-2){
                Log.i("CarAllFragment", "onActivityResult - ID recebido " + id );
            }
        }
    }








    //-----------------------------------------------------------------------------------------------------
    //---------------------------------RecyclerViewTouchListener-------------------------------------------
    //-----------------------------------------------------------------------------------------------------

    private static class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener {

        private Context mContext;
        private GestureDetector mGestureDetector;
        private RecyclerOnClickListener mRecyclerOnClickListener;

        public RecyclerViewTouchListener(Context mContext, final RecyclerView rv, final RecyclerOnClickListener mRecyclerOnClickListener) {
            this.mContext = mContext;
            this.mRecyclerOnClickListener = mRecyclerOnClickListener;

            mGestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener(){
              /*  @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);

                    View v = rv.findChildViewUnder(e.getX(), e.getY());
                    if(v != null && mRecyclerOnClickListener != null){
                        mRecyclerOnClickListener.onClickLongListener(v, rv.getChildAdapterPosition(v));
                    }


                }
                */
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    View v = rv.findChildViewUnder(e.getX(), e.getY());
                    if(v != null && mRecyclerOnClickListener != null){
                        mRecyclerOnClickListener.onClickListener(v, rv.getChildAdapterPosition(v));
                    }
                    return true;
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            mGestureDetector.onTouchEvent(e);
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }



}
